from Modelos.Inscripcion import Inscripcion
from Modelos.Materia import Materia
from Modelos.Estudiante import Estudiante
from Repositorios.RepositorioInscripcion import RepositorioInscripcion
from Repositorios.RepositorioMateria import RepositorioMateria
from Repositorios.RepositorioEstudiante import RepositorioEstudiante


class ControladorInscripcion():
    def __init__(self):
        self.repositorioInscripcion = RepositorioInscripcion()
        self.repositorioEstudiante = RepositorioEstudiante()
        self.repositorioMateria = RepositorioMateria()

    def index(self):
        return self.repositorioInscripcion.findAll()

    def create(self, infoInscripcion):
        nuevaInscripcion = Inscripcion(infoInscripcion)
        return self.repositorioInscripcion.save(nuevaInscripcion)

    def show(self, id):
        lainscripcion = Inscripcion(self.repositorioInscripcion.findById(id))
        return lainscripcion.__dict__

    def update(self, id, infoInscripcion):
        inscripcionActual = Inscripcion(self.repositorioInscripcion.findById(id))
        inscripcionActual.anio = infoInscripcion["año"]
        inscripcionActual.semestre = infoInscripcion["semestre"]
        inscripcionActual.nota_final = infoInscripcion["nota_final"]
        return self.repositorioInscripcion.save(inscripcionActual)

    def delete(self, id):
        return self.repositorioInscripcion.delete(id)

    """
    Asignacion estudiante y materia a inscripcion
    """

    def create(self,infoInscripcion,id_estudiante,id_materia):
        nuevaInscripcion = Inscripcion(infoInscripcion)
        elEstudiante = Estudiante(self.repositorioEstudiante.findById(id_estudiante))
        laMateria = Materia(self.repositorioMateria.findById(id_materia))
        nuevaInscripcion.estudiante = elEstudiante
        nuevaInscripcion.materia = laMateria
        return self.repositorioInscripcion.save(nuevaInscripcion)



    """
    Modificaciones de inscripcion (estudiante y materia) 
    """

    def update(self,id,infoInscripcion, id_estudiante,id_materia):
        laInscripcion= Inscripcion(self.repositorioInscripcion.findById(id))
        laInscripcion.anio = infoInscripcion["año"]
        laInscripcion.semestre = infoInscripcion["semestre"]
        laInscripcion.notaFinal = infoInscripcion["nota_final"]
        elEstudiante = Estudiante(self.repositorioEstudiante.findById(id_estudiante))
        laMateria = Materia(self.repositorioMateria.findById(id_materia))

        laInscripcion.estudiante = elEstudiante
        laInscripcion.materia = laMateria

        return self.repositorioInscripcion.save(laInscripcion)

    """
    Obtener todos los iscritos de una materia
    """
    def listarIscritosEnMateria(self, id_materia):
        return self.repositorioInscripcion.getListadoInscripcionMateria(id_materia)

    "Obtener notas mas altas por materia"

    def notasMasAltasPorCurso(self):
        return self.repositorioInscripcion.getMayorNotaPorCurso()

    "Obtener promedio de notas en materia"

    def promedioNotasEnMateria(self, id_materia):
        return self.repositorioInscripcion.promedioNotasEnMateria(id_materia)