from Modelos.Materia import Materia
from Modelos.Departamento import Departamento
from Repositorios.RepositorioMateria import RepositorioMateria
from Repositorios.RepositorioDepartamento import RepositorioDepartamento


class ControladorMateria():
    def __init__(self):
        self.repositorioMateria = RepositorioMateria()
        self.repositorioDepartamento = RepositorioDepartamento()

    def index(self):
        return self.repositorioMateria.findAll()

    def create(self, infoMateria):
        nuevaMateria = Materia(infoMateria)
        return self.repositorioMateria.save(nuevaMateria)

    def show(self, id):
        laMateria = Materia(self.repositorioMateria.findById(id))
        return laMateria.__dict__

    def update(self, id, infoMateria):
        materiaactual = Materia(self.repositorioMateria.findById(id))
        materiaactual.nombre = infoMateria["nombre"]
        materiaactual.creditos = infoMateria["creditos"]
        return self.repositorioMateria.save(materiaactual)

    def delete(self, id):
        return self.repositorioMateria.delete(id)

    """
    Relación  departamento y materia
    """
    def asignarDepartamento(self,id,id_departamento):
        materiaActual=Materia(self.repositorioMateria.findById(id))
        departamentoActual = Departamento(self.repositorioDepartamento.findById(id_departamento))
        materiaActual.departamento=departamentoActual
        return self.repositorioMateria.save(materiaActual)
